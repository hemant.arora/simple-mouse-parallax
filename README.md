# Simple Mouse Parallax

**Author:** Hemant Arora
**Company:** TivLabs

This jQuery plugin provides the ability to add a mouse hover based parallax
effect to the elements in selection.

## Usage

#### HTML Elements

The attributes `data-mpfactor` and `data-mpzoom` (optional) can be used to help
the script identify the child elements.

`data-mpfactor` - the intensity of displacement from original (center) position,
higher value will get displaced more.

`data-mpzoom` (optional) - to zoom the child element. This is useful with a
background element and helps to prevent it from displaying edges/limits when the
element moves around, a value set to, say 1.2, means 1.2x zoom.

```php
<div class="my-parallax-element">
	<div  data-mpfactor="2" data-mpzoom="1.2"></div>
	<div  data-mpfactor="7"></div>
	<div data-mpfactor="10"></div>
	<div data-mpfactor="15"></div>
</div>

```

#### Javascript

```javascript
jQuery('.my-parallax-element').smparallax();

```

## Options

A set of options can be passed to the call:

```javascript

jQuery(...).smparallax({
	reset: false // if true, resets everything when mouse exits parent element
});

```