/**
 * Simple Mouse Parallax
 * ---------------------
 * A simple implementation of the popular parallax effect
 * on mouse hover.
 * ------------------------------------------------------
 * Author: Hemant Arora
 * Website: https://gitlab.com/hemant.arora/simple-mouse-parallax
 * License: MIT
 * Version: 0.1
 * Last Update: Jun 08, 2017
 **/

(function ($) {

	$.fn.smparallax = function(options) {
		
		var settings = $.extend({
				soft_parallax: false,    // if true, parallax gets applied with easing transition when the mouse halts moving
				reset:         false     // if true, resets all changes to default when the mouse exits the parent element
			}, options),
			get_mouse_event_xy = function(event) {
				var eventDoc, doc, body, pageX, pageY;
				event = event || window.event; // IE-ism
				if (event.pageX == null && event.clientX != null) {
					eventDoc = (event.target && event.target.ownerDocument) || document;
					doc = eventDoc.documentElement;
					body = eventDoc.body;
					event.pageX = event.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
					event.pageY = event.clientY + (doc && doc.scrollTop  || body && body.scrollTop  || 0) - (doc && doc.clientTop  || body && body.clientTop  || 0 );
				}
				return event;
			},
			mouse_parallaxify_children = function($parent_element, $children, event) {
				event = get_mouse_event_xy(event);
				
				var center = { x: Math.floor($parent_element.width()/2), y: Math.floor($parent_element.height()/2) },
					mouse = { x: event.pageX - $parent_element.offset().left, y: event.pageY - $parent_element.offset().top },
					distance = { x: (mouse.x - center.x), y: (mouse.y - center.y) },
					abs_distance = { x: Math.abs(distance.x), y: Math.abs(distance.y) };
				
				$children.each(function(i, child) {
					if(settings.soft_parallax == true) {
						$(child).animate({
							marginLeft: (-1) * (distance.x/100) * $(child).attr('data-mpfactor'),
							marginTop:  (-1) * (distance.y/100) * $(child).attr('data-mpfactor')
						}, 'fast');
					} else {
						$(child).css({
							marginLeft: (-1) * (distance.x/100) * $(child).attr('data-mpfactor'),
							marginTop:  (-1) * (distance.y/100) * $(child).attr('data-mpfactor')
						});
					}
				}); //console.clear(); console.log('center', center, 'mouse', mouse, 'abs_distance', abs_distance);
				
			},
			delay_func = (function(){
				var timer = 0;
				return function(callback, ms){
					clearTimeout (timer);
					timer = setTimeout(callback, ms);
				};
			})();
		
		    this.each(function(index, parent_element) {
				$(parent_element)
					.mousemove(function(e) {
						if(settings.soft_parallax == true) {
							delay_func(function() {
								if($(parent_element).is(':hover')) {
									mouse_parallaxify_children($(parent_element), $(parent_element).find('[data-mpfactor]'), e);
								}
							}, 100);
						} else {
							mouse_parallaxify_children($(parent_element), $(parent_element).find('[data-mpfactor]'), e);
						}
					})
					.mouseout(function(e) {
						if(settings.reset == true)
							$(parent_element).find('[data-mpfactor]').each(function(i, child) { $(child).css({ marginTop: 0, marginLeft: 0 }); });
					});
				$(parent_element).find('[data-mpfactor][data-mpzoom]')
					.each(function(i, child_element) {
						var transform = $(child_element).css('transform');
						transform = typeof(transform) == 'undefined' || transform == null ? '' : transform;
						if(transform.indexOf('scale(') > -1) {
							transform = transform.replace(/scale\(([^,]+)[^)]*\)/g, 'scale('+$(child_element).attr('data-mpzoom')+')');
						} else {
							transform = (transform == '' || transform == 'none' ? '' : transform+' ')+'scale('+$(child_element).attr('data-mpzoom')+')';
						}
						$(child_element).css('transform', transform);
					});
			});
			return this;
		
	};

}(jQuery));